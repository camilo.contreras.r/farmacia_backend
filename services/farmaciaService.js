var request = require('request');

function getLocalesRegion(regId, callback) {
    var options = {
        uri : 'https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region='+regId,
        method : 'GET'
    }; 
    var res = '';
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res = body;
        }
        else {
            res = 'Not Found';
        }
        callback(res);
    });
};

function getComunasRegion(regId, callback) {
    var options = {
        uri : 'https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones',
        method : 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        form: {reg_id : regId}
    }; 
    var res = '';
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body);
            res = body;
        }
        else {
            res = 'Not Found';
        }
        callback(res);
    });
};

module.exports = {
    getLocalesRegion: getLocalesRegion,
    getComunasRegion: getComunasRegion
};