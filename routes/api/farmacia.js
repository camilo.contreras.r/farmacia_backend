// routes/api/farmacia.js

const express = require('express');
const router = express.Router();

// Load Farmacia model
const farmaciaService = require('../../services/farmaciaService');

// @route GET api/farmacia/test
// @description tests farmacias route
// @access Public
router.get('/test', (req, res) => res.send('farmacia route testing!'));

// @route GET api/farmacia
// @description getLocalesRegion
// @access Public
router.get('/getLocalesRegion', (req, res) => {
    farmaciaService.getLocalesRegion(req.query.regId, function(resp){
        if(resp !== '[]'){
            res.status(200).send(resp);
        }else {
            res.status(404).json({ nofarmaciafound: 'No Farmacias found' });
        }
    });
});

// @route GET api/farmacia
// @description getRegions
// @access Public
router.get('/getRegions', (req, res) => {
    var regiones = [
        {
         "id": 1,
         "nombreRegion":"REGION DE TARAPACA",
         "ordenPlan": 1
       },
       { "id": 2,
         "nombreRegion":"REGION DE ANTOFAGASTA",
         "ordenPlan": 2
       },
       { "id": 3,
         "nombreRegion":"REGION DE ATACAMA",
         "ordenPlan": 3
       },
       { "id": 4,
         "nombreRegion":"REGION DE COQUIMBO",
         "ordenPlan": 4
       },
       { "id": 5,
         "nombreRegion":"REGION DE VALPARAISO",
         "ordenPlan": 5
       },
       { "id": 6,
         "nombreRegion":"REGION DEL LIBERTADOR BERNARDO OHIGGINS",
         "ordenPlan": 6
       },
       { "id": 7,
         "nombreRegion":"REGION DE MAULE",
         "ordenPlan": 7
       },
       { "id": 8,
         "nombreRegion":"REGION DEL BIO BIO",
         "ordenPlan": 8
       },
       { "id": 9,
         "nombreRegion":"REGION DE LA ARAUCANIA",
         "ordenPlan": 9
       },
       { "id": 10,
         "nombreRegion":"REGION DE LOS LAGOS",
         "ordenPlan": 10
       },
       { "id": 11,
         "nombreRegion":"REGION DE AYSEN DEL GENERAL CARLOS IBAÑEZ DEL CAMPO",
         "ordenPlan": 11
       },
       { "id": 12,
         "nombreRegion":"REGION DE MAGALLANES Y ANTARTICA CHILENA",
         "ordenPlan": 12
       },
 
       { "id": 13,
         "nombreRegion":"REGION METROPOLITANA DE SANTIAGO",
         "ordenPlan": 13
       },
 
       { "id": 14,
         "nombreRegion":"REGION DE LOS RIOS",
         "ordenPlan": 14
       },
 
       { "id": 15,
         "nombreRegion":"REGION DE ARICA Y PARINACOTA",
         "ordenPlan": 15
       },
       { "id": 16,
         "nombreRegion":"REGION DE ÑUBLE",
         "ordenPlan": 16
       }
    ];
    res.status(200).json(regiones);
});

// @route GET api/farmacia
// @description maps_obtener_comunas_por_regiones
// @access Public
router.get('/getComunasByRegion', (req, res) => {
    console.log(req.query.regId);
    farmaciaService.getComunasRegion(req.query.regId, function(resp){
        var r = resp.split('</option><');
        console.log(r);
        res.status(200).json(resp);
    });
});


module.exports = router;