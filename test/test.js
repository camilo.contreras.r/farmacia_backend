let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
const app = require('../index');
chai.use(chaiHttp);
const url= 'http://localhost:3001';


describe('getLocalesRegion: ',()=>{

	it('should list locales by region', (done) => {
		chai.request(app)
			.get('/api/farmacia/getLocalesRegion')
			.query({regId: 7})
			.end( function(err,res){
				//console.log(err)
				//console.log(res)
				expect(res).to.have.status(200);
				done();
			});
	});
});


describe('getRegions: ',()=>{

	it('should list regiones', (done) => {
		chai.request(app)
			.get('/api/farmacia/getRegions')
			.end( function(err,res){
				//console.log(err)
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});

describe('getComunasByRegion: ',()=>{

	it('should list comunas', (done) => {
		chai.request(app)
			.get('/api/farmacia/getComunasByRegion')
			.query({regId: 7})
			.end( function(err,res){
				//console.log(err)
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});
