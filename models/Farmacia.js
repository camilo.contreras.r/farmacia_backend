// models/Book.js

const mongoose = require('mongoose');

const FarmaciaSchema = new mongoose.Schema({
  fecha: {
    type: String,
    required: true
  },
  local_id: {
    type: String,
    required: true
  },
  local_nombre: {
    type: String,
    required: true
  },
  comuna_nombre: {
    type: String,
    required: true
  },
  localidad_nombre: {
    type: String,
    required: true
  },
  local_direccion: {
    type: String,
    required: true
  },
  local_direccion: {
    type: String,
    required: true
  },
  funcionamiento_hora_apertura: {
    type: String,
    required: true
  },
  funcionamiento_hora_cierre: {
    type: String,
    required: true
  },
  local_telefono: {
    type: String,
    required: true
  },
  local_lat: {
    type: String,
    required: true
  },
  local_lng: {
    type: String,
    required: true
  },
  funcionamiento_dia: {
    type: String,
    required: true
  },
  fk_region: {
    type: String,
    required: true
  },
  fk_comuna: {
    type: String,
    required: true
  }
});

module.exports = Farmacia = mongoose.model('farmacia', FarmaciaSchema);