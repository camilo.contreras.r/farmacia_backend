FROM node:alpine

# CONSUL
#ARG CONSUL_TEMPLATE_VERSION=0.19.4
#RUN wget "https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz"
#RUN tar zxfv consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.tgz


RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python


RUN \
  echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
  && echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories \
  && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
  && apk --no-cache  update \
  && apk --no-cache  upgrade \
  && apk add --no-cache --virtual .build-deps \
    gifsicle pngquant optipng libjpeg-turbo-utils \
    udev ttf-opensans chromium \
  && rm -rf /var/cache/apk/* /tmp/*


ENV CHROME_BIN /usr/bin/chromium-browser
ENV LIGHTHOUSE_CHROMIUM_PATH /usr/bin/chromium-browser

WORKDIR /home/app

COPY . .


RUN npm install --quiet

#RUN npm run seed

EXPOSE 3000
#CMD  ["/bin/sh", "./docker/entrypoint.sh"]
#CMD ["node", "./dist/seeder/index.js"]


CMD ["node" ,"./index.js"]

#docker build -t farmacia_backend .
#docker run -p 3000:3000 farmacia_backend
#docker exec -it a41108aeca2a  ls ./assets
#docker container ls -a
#docker container stop id_container
#docker container rm id_container
